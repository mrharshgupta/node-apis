const express = require("express");
const app = express();
app.use(express.json());

var { createSubdomain } = require("./src/aws/create-subdomain.js");
var { books } = require("./src/aws/cred.js");

//READ Request Handlers
app.get("/", (req, res) => {
  res.send("Welcome to Onitt!!");
});

app.get("/apis/domains/create-subdomain", (req, res) => {
  createSubdomain(req, res);
});

//PORT ENVIRONMENT VARIABLE
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}..`));
