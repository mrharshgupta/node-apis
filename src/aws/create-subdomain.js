var AWS = require("aws-sdk");
const { awsCreds } = require("./cred.js");

const createSubdomain = (req, res) => {
  const subdomainName = req.query.subdomainName;
  var route53 = new AWS.Route53({
    accessKeyId: awsCreds.accessKey,
    secretAccessKey: awsCreds.secretAccessKey,
  });

  var params = {
    ChangeBatch: {
      Changes: [
        {
          Action: "CREATE",
          ResourceRecordSet: {
            AliasTarget: {
              DNSName: "dualstack.onitt-515066756.us-east-2.elb.amazonaws.com",
              EvaluateTargetHealth: true,
              HostedZoneId: "Z3AADJGX6KTTL2",
            },
            Name: subdomainName,
            Type: "A",
          },
        },
      ],
      Comment: "Testing subdomain creation",
    },
    HostedZoneId: "Z07182772MVLR6ET1V6WA", // Depends on the type of resource that you want to route traffic to
  };
  route53.changeResourceRecordSets(params, function (err, data) {
    if (err) {
      console.log(err, err.stack); // an error occurred
      res.send("error");
    } else {
      console.log(data);
      res.send("done");
    }
  });
};

module.exports = { createSubdomain };
